# Maintainer: Dominika Liberda <ja@sdomi.pl>
# Contributor: Dominika Liberda <ja@sdomi.pl>
pkgname=texlab
pkgver=4.3.1
pkgrel=0
pkgdesc="An implementation of the Language Server Protocol for LaTeX"
url="https://github.com/latex-lsp/texlab"
# limited by rust/cargo
# armhf - fails to build
arch="x86_64 armv7 aarch64 x86 ppc64le"
license="GPL-3.0-or-later"
makedepends="cargo"
source="https://github.com/latex-lsp/texlab/archive/refs/tags/v$pkgver/texlab-v$pkgver.tar.gz"

# tests OOM on 32-bit
# x86_64/ppc64le tests broken with some things in /tmp
case "$CARCH" in
	x86|x86_64|ppc64le|armv7) options="!check" ;;
esac

export CARGO_PROFILE_RELEASE_PANIC="unwind"

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	cargo build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/texlab -t "$pkgdir"/usr/bin/
}

sha512sums="
eeb49a419fa631e6a06e2e5d14941d6c8709ec7a299381b6ed185011668cd9894688d668e66a580e2230a316d5cfb2b3de38c96b45adf5e5431882b039cb115d  texlab-v4.3.1.tar.gz
"
