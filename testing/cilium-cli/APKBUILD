# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=cilium-cli
pkgver=0.12.7
pkgrel=0
pkgdesc="CLI to install, manage and troubleshoot Kubernetes clusters running Cilium"
url="https://cilium.io/"
arch="all"
license="Apache-2.0"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/cilium/cilium-cli/archive/refs/tags/v$pkgver.tar.gz"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v \
		-ldflags "-X github.com/cilium/cilium-cli/internal/cli/cmd.Version=v$pkgver" \
		-o cilium \
		./cmd/cilium

	for shell in bash fish zsh; do
		./cilium completion $shell > cilium.$shell
	done
}

check() {
	go test ./...
}

package() {
	install -Dm755 cilium -t "$pkgdir"/usr/bin/

	install -Dm644 cilium.bash \
		"$pkgdir"/usr/share/bash-completion/completions/cilium
	install -Dm644 cilium.fish \
		"$pkgdir"/usr/share/fish/completions/cilium.fish
	install -Dm644 cilium.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_cilium
}

sha512sums="
d0dd2cd1c2eaf6b4a7da3d2fdf09a9e4e22cf412c0d7bbb964e91b295b59fcdb946d6dbf94d35b7eaf56dc839699b018f74ecbf64533e282aa9083260810b6ed  cilium-cli-0.12.7.tar.gz
"
