# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=kubeseal
pkgver=0.19.2
pkgrel=0
pkgdesc="One-way encrypted Secrets tool for Kubernetes"
url="https://sealed-secrets.netlify.app/"
arch="all"
license="Apache-2.0"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/bitnami-labs/sealed-secrets/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/sealed-secrets-$pkgver"

# secfixes:
#   0.19.0-r0:
#     - CVE-2022-32149

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v \
		-o bin/$pkgname \
		-ldflags "-X main.VERSION=$pkgver" \
		./cmd/kubeseal
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/$pkgname -t "$pkgdir"/usr/bin
}

sha512sums="
86556e9a738edfabcf8216b678665f2b34259025ec79e4314952f3e749a79ae4683e28890b91a63016b6376688ed7e1fa906518dd38940ff24a38ad2da0f6f75  kubeseal-0.19.2.tar.gz
"
