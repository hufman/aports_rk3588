# Contributor: Hoang Nguyen <folliekazetani@protonmail.com>
# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=helmfile
pkgver=0.148.1
pkgrel=0
pkgdesc="Declarative spec for deploying helm charts"
url="https://helmfile.readthedocs.io/"
arch="all"
license="MIT"
depends="helm"
makedepends="go"
checkdepends="bash"
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/helmfile/helmfile/archive/refs/tags/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	local _goldflags="
	-X go.szostok.io/version.version=$pkgver
	-X go.szostok.io/version.buildDate=$(date -u "+%Y-%m-%dT%H:%M:%S%z" ${SOURCE_DATE_EPOCH:+-d @$SOURCE_DATE_EPOCH})
	-X go.szostok.io/version.commitDate=$(date -u "+%Y-%m-%dT%H:%M:%S%z" ${SOURCE_DATE_EPOCH:+-d @$SOURCE_DATE_EPOCH})
	-X go.szostok.io/version.commit=0000000
	-X go.szostok.io/version.dirtyBuild=false
	"
	go build -v -o helmfile -ldflags "$_goldflags"

	for shell in bash fish zsh; do
		./helmfile completion $shell > $pkgname.$shell
	done
}

check() {
	# e2e/template test requires docker
	# NOTE: run tests for `tmpl` module again in the next release
	# (https://github.com/helmfile/helmfile/blob/6a36f34c7db20e95fdac16bd835c4751012ceb16/pkg/tmpl/context_funcs_test.go#L361)
	go test $(go list ./... | grep -v /e2e | grep -v /tmpl)
}

package() {
	install -Dm755 $pkgname "$pkgdir"/usr/bin/$pkgname

	install -Dm644 helmfile.bash \
		"$pkgdir"/usr/share/bash-completion/completions/helmfile
	install -Dm644 helmfile.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_helmfile
	install -Dm644 helmfile.fish \
		"$pkgdir"/usr/share/fish/completions/helmfile.fish
}

sha512sums="
8ca602167957ba34c6a076cc79e3ad8240b251bb0026f7727e516bb1161b9215ea60ee273586118699fc3b582b7c2e2ff7d2e87f0032b580953d7a3013ce8dd9  helmfile-0.148.1.tar.gz
"
