# Contributor: Aiden Grossman <agrossman154@yahoo.com>
# Maintainer: Aiden Grossman <agrossman154@yahoo.com>
pkgname=jupyter-server
pkgver=1.23.2
pkgrel=0
pkgdesc="Backend for Jupyter web applications"
url="https://github.com/jupyter-server/jupyter_server"
# s390x: no py3-argon2-cffi
# ppc64le, armhf: no py3-anyio
arch="noarch !s390x !ppc64le !armhf"
license="BSD-3-Clause"
depends="
	py3-jupyter-packaging
	py3-anyio
	py3-argon2-cffi
	py3-jinja2
	py3-jupyter_client
	py3-jupyter_core
	jupyter-nbconvert
	jupyter-nbformat
	py3-packaging
	py3-prometheus-client
	py3-send2trash
	py3-terminado
	py3-tornado
	py3-traitlets
	py3-websocket-client
	"
makedepends="py3-gpep517 py3-installer py3-setuptools py3-wheel"
checkdepends="
	py3-pytest
	py3-pytest-tornasync
	py3-requests
	py3-pytest-timeout
	py3-ipykernel
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/jupyter-server/jupyter_server/releases/download/v$pkgver/jupyter_server-$pkgver.tar.gz
	pyproject.patch"
builddir="$srcdir/jupyter_server-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	# deselect test requires another pytest dependency to be packaged
	pytest \
		--disable-warnings \
		--deselect tests/extension/test_entrypoint.py::test_server_extension_list
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
12b0dc05e49f18e14835bb9de08e41169157dc0861b66a143a1009dd78d106a513a5f58f4c4a1992d6423fd8929babadd8a7b13dcad4d26c4126fcb413f92791  jupyter-server-1.23.2.tar.gz
f2b4162e72d62d28fd50c2f02bf957e73c7e916e88bb85bd9d98a3b3373103a3f12275d61d5a96dcb5075ba7cb54b893572205d15116c49d6d6a03241b2813c6  pyproject.patch
"
