# Contributor: Chloe Kudryavtsev <code@toast.bunkerlabs.net>
# Maintainer:
pkgname=rclone
pkgver=1.60.1
pkgrel=0
pkgdesc="Rsync for cloud storage"
url="https://rclone.org/"
# riscv64 blocked by binutils-gold
arch="all !riscv64"
license="MIT"
makedepends="binutils-gold go"
checkdepends="fuse"
options="!check net" # tests fail in CI due filesystem access
source="rclone-$pkgver.tar.gz::https://github.com/rclone/rclone/archive/refs/tags/v$pkgver.tar.gz"
subpackages="$pkgname-doc $pkgname-bash-completion $pkgname-fish-completion $pkgname-zsh-completion"

export GO111MODULE=on
export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build

	"$builddir"/rclone genautocomplete bash rclone.bash
	"$builddir"/rclone genautocomplete fish rclone.fish
	"$builddir"/rclone genautocomplete zsh rclone.zsh
}

check() {
	# backend/{ftp,sftp,swift,webdav,seafile}: uses docker(1) for tests
	# backend/{local,memory}: fails in docker envs, but not on real machines
	# fs/rc: fails on ppc64le
	# shellcheck disable=SC2046
	go test $(go list ./... | grep -v \
		-e 'backend/ftp$' \
		-e 'backend/local$' \
		-e 'backend/memory$' \
		-e 'backend/sftp$' \
		-e 'backend/swift$' \
		-e 'backend/webdav$' \
		-e 'backend/seafile$' \
		-e 'fs/rc$'
		)
}

package() {
	install -Dm755 "$builddir"/rclone \
		"$pkgdir"/usr/bin/rclone

	mkdir -p "$pkgdir"/sbin
	ln -s /usr/bin/rclone "$pkgdir"/sbin/mount.rclone
	ln -s /usr/bin/rclone "$pkgdir"/usr/bin/rclonefs

	install -Dm644 "$builddir"/rclone.1 \
		"$pkgdir"/usr/share/man/man1/rclone.1

	install -Dm644 "$builddir"/rclone.bash \
		"$pkgdir"/usr/share/bash-completion/completions/rclone

	install -Dm644 "$builddir"/rclone.fish \
		"$pkgdir"/usr/share/fish/completions/rclone.fish

	install -Dm644 "$builddir"/rclone.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_rclone
}

cleanup_srcdir() {
	go clean -modcache
	default_cleanup_srcdir
}

sha512sums="
ba4e4a3a57ae639665ca22576a4c8ed6d8818aa903a7d4211bf2cfbb0fb81a95162521806b7174a042bb684262786dfee4c204d6080d33e59bd9705dc9d8db9c  rclone-1.60.1.tar.gz
"
