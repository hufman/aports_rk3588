# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=networkmanager-qt
pkgver=5.100.0
pkgrel=0
pkgdesc="Qt wrapper for NetworkManager API"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends="networkmanager"
depends_dev="networkmanager-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/networkmanager-qt-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# The excluded tests currently fail
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(manager|settings|activeconnection)test'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
60a2ba7ac18d80035b3257097a9f7b130ebc6f0e8655bb56b7e14084ba99e869911f43836f7a3d981e98e3e69394b737a99a9d392fd6c7cb54f7699b6c276a74  networkmanager-qt-5.100.0.tar.xz
"
