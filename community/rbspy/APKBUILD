# Contributor: Adam Jensen <adam@acj.sh>
# Maintainer: Adam Jensen <adam@acj.sh>
pkgname=rbspy
pkgver=0.13.1
pkgrel=0
pkgdesc="Sampling profiler for Ruby"
url="https://rbspy.github.io/"
arch="all !armv7 !ppc64le !s390x !riscv64" # limited by cargo and build errors
license="MIT"
makedepends="cargo"
checkdepends="ruby"
options="net"
source="$pkgname-$pkgver.tar.gz::https://github.com/rbspy/rbspy/archive/v$pkgver.tar.gz"

build() {
	cargo build --release --locked
}

check() {
	# Some tests need additional privileges
	cargo test --release --locked -- \
		--skip sampler::tests \
		--skip test_current_thread_address \
		--skip test_initialize_with_disallowed_process \
		--skip test_get_exec_trace \
		--skip test_get_trace
}

package() {
	install -Dm755 "target/release/rbspy" "$pkgdir/usr/bin/rbspy"
}

sha512sums="
f439012d040bc2eca21204ad3707df7188dd19b66b106ff1b170e8f5de99b71a408b5e60c5cb25a5eeb47dbbacbed748c261a07a8e87bac37d4e09bbd78bb740  rbspy-0.13.1.tar.gz
"
