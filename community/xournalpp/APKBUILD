# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=xournalpp
pkgver=1.1.2
pkgrel=0
pkgdesc="Xournal++ is a handwriting notetaking software with PDF annotation support"
url="https://github.com/xournalpp/xournalpp"
arch="all !s390x !riscv64"
license="GPL-2.0-or-later"
makedepends="
	cmake
	glib-dev
	gtk+3.0-dev
	librsvg-dev
	libsndfile-dev
	libx11-dev
	libxi-dev
	libxml2-dev
	libzip-dev
	lsb-release
	lua5.3-dev
	poppler-dev
	portaudio-dev
	samurai
	"
options="!check" # no tests
subpackages="$pkgname-dbg $pkgname-lang"
source="xournalpp-$pkgver.tar.gz::https://github.com/xournalpp/xournalpp/archive/v$pkgver.tar.gz
	no-execinfo.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi

	# Increase stack-size to avoid crashes when using pen for input
	LDFLAGS="$LDFLAGS -Wl,-z,stack-size=2097152" \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		$CMAKE_CROSSOPTS
	cmake --build build --target all translations
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
51022df1750390c17eb57a77e37c4224f227728aa49f90c9733a68c1f6c3c06f2e2634cb0b70add9a1d92525afe50fe317c12b879d2e5ed0f954360145359a89  xournalpp-1.1.2.tar.gz
0aa160f6848f566efeaf5a4bdb1eca4f2858ac1a1b4f6931dabce8dc8c9790cfd5680bbe4527d4cbbdc8e0f1fa633e43e3e45f9e61d0b60fb38a18eeec620d80  no-execinfo.patch
"
