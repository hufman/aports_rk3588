# Contributor: Rob Blanckaert <basicer@gmail.com>
# Maintainer: Rob Blanckaert <basicer@gmail.com>
pkgname=luau
pkgver=0.554
pkgrel=0
pkgdesc="A fast, small, safe, gradually typed embeddable scripting language derived from Lua"
url="https://github.com/roblox/luau"
arch="all"
license="MIT"
makedepends="cmake linux-headers samurai"
source="
$pkgname-$pkgver.tar.gz::https://github.com/Roblox/luau/archive/refs/tags/$pkgver.tar.gz
"



build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	"$builddir"/build/Luau.UnitTest && "$builddir"/build/Luau.Conformance
}

package() {
	install -Dm755 build/luau "$pkgdir"/usr/bin/luau
	install -Dm755 build/luau-analyze "$pkgdir"/usr/bin/luau-analyze
}

sha512sums="
4f244b7e4f05a1be236b1f5fbcaf23b6dfec0b5ae876e89c9210685c7a9bf7705b5b14d460331671cad9aab34d39859d3fe4b536169682f8d7e5e7526b0220fd  luau-0.554.tar.gz
"
