# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwayland
pkgver=5.100.0
pkgrel=0
pkgdesc="Qt-style Client and Server library wrapper for the Wayland libraries"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://www.kde.org"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	qt5-qtwayland-dev
	wayland-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	plasma-wayland-protocols
	qt5-qtbase-dev
	qt5-qttools-dev
	wayland-protocols
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwayland-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-dbg"
options="!check" # Fails due to requiring running Wayland compositor

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
270b28d23302d5ab90a90894a5d903a19d60dc9b5a52c124af03de3ec7910826cdec130c2b3c09f1808f33080979f6f4ac608d037e32975e9495f3c9a18b929c  kwayland-5.100.0.tar.xz
"
