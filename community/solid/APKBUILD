# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=solid
pkgver=5.100.0
pkgrel=0
pkgdesc="Hardware integration and detection"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="qt5-qtdeclarative-dev"
makedepends="$depends_dev
	extra-cmake-modules
	bison
	doxygen
	eudev-dev
	flex-dev
	qt5-qttools-dev
	samurai
	udisks2-dev
	upower-dev
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/solid-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-libs $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# solidmttest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "solidmttest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7323859c0ef59911ec2fb76f56e79b5b6a1ac0f5c58630114748a3eb1c39f33faf7291b83f341146125416dd08e35ae4f86d2e97dfdec1d94a5beb66d445d43e  solid-5.100.0.tar.xz
"
