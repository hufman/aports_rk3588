# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=py3-pytest-asyncio
pkgver=0.20.2
pkgrel=0
pkgdesc="Pytest support for asyncio"
url="https://github.com/pytest-dev/pytest-asyncio"
arch="noarch"
license="Apache-2.0"
depends="python3 py3-flaky py3-pytest py3-pytest-trio"
makedepends="py3-setuptools py3-setuptools_scm py3-gpep517 py3-installer py3-wheel"
checkdepends="py3-async_generator py3-coverage py3-hypothesis"
source="$pkgname-$pkgver.tar.gz::https://github.com/pytest-dev/pytest-asyncio/archive/v$pkgver.tar.gz"
builddir="$srcdir/pytest-asyncio-$pkgver"
options="!check" # fail for some reason

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	python3 -m venv --system-site-packages test-env
	test-env/bin/python3 -m installer dist/pytest_asyncio-*.whl
	test-env/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/pytest_asyncio-*.whl
}

sha512sums="
5d8b4965cea14dec2a2c7f21f6bb1bad6693b93f07b9dab5c880107a944942bd2c3ef38adc0e17b309222a53afa7504f827baf42201c2a0282a266118e3f7eb7  py3-pytest-asyncio-0.20.2.tar.gz
"
