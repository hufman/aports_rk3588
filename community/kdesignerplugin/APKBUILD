# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdesignerplugin
pkgver=5.100.0
pkgrel=0
pkgdesc="Integration of Frameworks widgets in Qt Designer/Creator"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only"
depends_dev="
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kiconthemes-dev
	kio-dev
	kitemviews-dev
	kplotting-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	sonnet-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	kdoctools-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kdesignerplugin-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
b2d4d9345c2108a0fd7e0c5bc67fa6bc52463bf5c59ac77e1eae2508480c9f91f0abaef00a0f79782bd0f7accf1ab677ca0f3637322d390c62e88e805e85331d  kdesignerplugin-5.100.0.tar.xz
"
